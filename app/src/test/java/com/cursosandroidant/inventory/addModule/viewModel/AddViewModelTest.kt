package com.cursosandroidant.inventory.addModule.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.cursosandroidant.inventory.entities.Product
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AddViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()


    @Test
    fun addProductTest() {
        val viewmodel = AddViewModel()
        val product = Product(
            117,
            "Xbox",
            20,
            "https://http2.mlstatic.com/D_NQ_NP_2X_764843-MLA45733462849_042021-V.webp",
            4.8, 56
        )
        val observer = Observer<Boolean> {}
        try {
            viewmodel.getResult().observeForever(observer)
            viewmodel.addProduct(product)
            val result = viewmodel.getResult().value
            assertThat(result, `is`(true))
            assertThat(result, not(nullValue()))
        } finally {
            viewmodel.getResult().removeObserver(observer)
        }

    }
}